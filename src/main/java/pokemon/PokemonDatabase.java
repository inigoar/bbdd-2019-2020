package pokemon;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;


public class PokemonDatabase {

    private static final Logger logger = LogManager.getLogger(PokemonDatabase.class);
    private Connection connection;

    public PokemonDatabase() {

    }

    public boolean connect() {
        try {
            if (connection != null && !connection.isClosed()) {
                return true;
            }

            Properties connectionProps = new Properties();
            connectionProps.put("user", "pokemon_user");
            connectionProps.put("password", "pokemon_pass");
            connectionProps.put("serverTimezone", "UTC");
            String serverAddress = "localhost:3306";
            String database = "pokemon";
            String url = "jdbc:mysql://" + serverAddress + "/" + database;

            connection = DriverManager.getConnection(url, connectionProps);

            logger.info("Connection established successfully");
            return true;

        } catch (SQLException e) {
            generateExceptionMessage(e);
            return false;
        }
    }

    public boolean disconnect() {
        try {
            if (connection == null || connection.isClosed()) return false;

            connection.close();
            logger.info("Connection closed successfully");
            return true;

        } catch (SQLException e) {
            generateExceptionMessage(e);
            return false;
        }
    }

    public boolean createTableAprende() {
        if (!connect()) return false;
        try (PreparedStatement pst = connection.prepareStatement("CREATE TABLE aprende (" +
                "N_Pokedex INT," +
                "ID_Ataque INT," +
                "Nivel INT," +
                "PRIMARY KEY (N_Pokedex,ID_Ataque)," +
                "FOREIGN KEY (N_Pokedex) REFERENCES especie (N_Pokedex)," +
                "FOREIGN KEY (ID_Ataque) REFERENCES ataque (ID_Ataque)" +
                "ON DELETE CASCADE ON UPDATE CASCADE);")) {
            pst.execute();
            return true;

        } catch (SQLException e) {
            generateExceptionMessage(e);
            return false;
        }
    }

    public boolean createTableConoce() {
        if (!connect()) return false;
        try (PreparedStatement pst = connection.prepareStatement("CREATE TABLE conoce (" +
                "N_Encuentro INT," +
                "N_Pokedex INT," +
                "ID_Ataque INT," +
                "PRIMARY KEY (N_Pokedex,N_Encuentro,ID_Ataque)," +
                "FOREIGN KEY (N_Pokedex,N_Encuentro) REFERENCES ejemplar (n_Pokedex,n_encuentro)," +
                "FOREIGN KEY  (ID_Ataque) REFERENCES  ataque (ID_Ataque)" +
                "ON DELETE CASCADE ON UPDATE CASCADE);")) {
            pst.execute();
            return true;

        } catch (SQLException e) {
            generateExceptionMessage(e);
            return false;
        }
    }

    public int loadAprende(String fileName) {
        if (!connect()) return 0;
        ArrayList<Aprende> listaAprende = Aprende.readData(fileName);
        if (listaAprende.isEmpty()) return 0;
        int res = 0;
        for (Aprende var : listaAprende) {
            try (PreparedStatement pst = connection.prepareStatement("INSERT IGNORE INTO aprende (N_pokedex, ID_Ataque, Nivel) " +
                    "VALUES (?,?,?);")) {
                pst.setInt(1, var.getId_especie());
                pst.setInt(2, var.getId_ataque());
                pst.setInt(3, var.getNivel());
                res = res + pst.executeUpdate();
            } catch (SQLException e) {
                generateExceptionMessage(e);
                return 0;
            }
        }
        return res;
    }

    public int loadConoce(String fileName) {
        if (!connect()) return 0;
        ArrayList<Conoce> listaConoce = Conoce.readData(fileName);
        if (listaConoce.isEmpty()) return 0;
        int res = 0;
        try (PreparedStatement pst = connection.prepareStatement("INSERT IGNORE INTO conoce (N_Encuentro, N_Pokedex, ID_Ataque) " +
                "VALUES (?,?,?);")) {
            connection.setAutoCommit(false);
            for (Conoce var : listaConoce) {
                pst.setInt(1, var.getN_encuentro());
                pst.setInt(2, var.getId_especie());
                pst.setInt(3, var.getId_ataque());
                res = res + pst.executeUpdate();
            }
            connection.commit();
            connection.setAutoCommit(true);
        } catch (SQLException throwables) {
            generateExceptionMessage(throwables);
            return 0;
        }
        return res;
    }

    public ArrayList<Especie> pokedex() {
        if (!connect()) return null;
        ArrayList<Especie> res = new ArrayList<>();
        try (PreparedStatement pst = connection.prepareStatement("SELECT * FROM especie;");
             ResultSet rst = pst.executeQuery()) {
            while (rst.next()) {
                Especie var = new Especie();
                var.setN_pokedex(rst.getInt("N_pokedex"));
                var.setNombre(rst.getString("Nombre"));
                var.setDescripcion(rst.getString("descripcion"));
                var.setEvoluciona(rst.getInt("evoluciona"));
                res.add(var);
            }
            return res;
        } catch (SQLException throwables) {
            generateExceptionMessage(throwables);
            return null;
        }
    }

    public ArrayList<Ejemplar> getEjemplares() {

        if (!connect()) return null;

        ArrayList<Ejemplar> resultado = new ArrayList<>();
        try (PreparedStatement pst = connection.prepareStatement("SELECT * FROM ejemplar ORDER BY N_Pokedex, N_Encuentro");
             ResultSet rst = pst.executeQuery()) {

            while (rst.next()) {
                Ejemplar var = new Ejemplar();
                var.setN_pokedex(rst.getInt("N_Pokedex"));
                var.setN_encuentro(rst.getInt("N_Encuentro"));
                var.setApodo(rst.getString("Apodo"));
                var.setSexo(rst.getString("Sexo").toCharArray()[0]);
                var.setNivel(rst.getInt("Nivel"));
                var.setInfectado(rst.getInt("Infectado"));
                resultado.add(var);

            }
            return resultado;
        } catch (SQLException throwables) {

            generateExceptionMessage(throwables);
            return null;

        }
    }

    public int coronapokerus(ArrayList<Ejemplar> ejemplares, int dias) {
        if (!connect()) return 0;
        int i = 0;
        int res = 0;
        while (i < dias) {
            try (PreparedStatement pst = connection.prepareStatement("SELECT COUNT(*) AS infectados FROM ejemplar WHERE infectado = 1;");
                 ResultSet rst = pst.executeQuery()) {
                while (rst.next()) {
                    connection.setAutoCommit(false);
                    if (rst.getInt("infectados") == 0) {
                        Ejemplar var = Ejemplar.ejemplarRandom(ejemplares);
                        var.setInfectado(1);
                        spread(var);
                    } else {
                        for (int j = rst.getInt("infectados"); j > 0; j--) {
                            Ejemplar var = Ejemplar.ejemplarRandom(ejemplares);
                            var.setInfectado(1);
                            spread(var);
                        }
                    }
                    connection.commit();
                }
            } catch (SQLException throwables) {
                generateExceptionMessage(throwables);
            }
            i++;
        }
        try (PreparedStatement pst = connection.prepareStatement("SELECT COUNT(*) AS infectados FROM ejemplar WHERE infectado = 1;");
             ResultSet rst = pst.executeQuery()) {
            connection.setAutoCommit(true);
            while (rst.next()) {
                res = rst.getInt("infectados");
            }
        } catch (SQLException throwables) {
            generateExceptionMessage(throwables);
        }
        return res;
    }

    public boolean getSprite(int n_pokedex, String filename) {
        if (!connect()) return false;
        ResultSet rst = null;
        FileOutputStream fileOutputStream = null;
        try (PreparedStatement pst = connection.prepareStatement("SELECT sprite FROM especie WHERE n_pokedex = ?;")) {
            pst.setInt(1, n_pokedex);
            rst = pst.executeQuery();
            byte[] data = null;
            Blob blob;
            while (rst.next()) {
                blob = rst.getBlob(1);
                if (blob == null) return false;
                data = blob.getBytes(1, (int) blob.length());
            }
            assert data != null;
            fileOutputStream = new FileOutputStream(filename);
            fileOutputStream.write(data);

        } catch (SQLException | IOException throwables) {
            generateExceptionMessage(throwables);
            return false;
        } finally {
            try {
                assert rst != null;
                rst.close();
                if (fileOutputStream != null) fileOutputStream.close();
            } catch (SQLException | IOException throwables) {
                generateExceptionMessage(throwables);
            }
        }
        return true;
    }

    private void spread(Ejemplar ejemplar) {
        try (PreparedStatement pst1 = connection.prepareStatement("UPDATE ejemplar SET infectado = ? " +
                "WHERE n_encuentro = ? AND n_pokedex = ?;")) {
            pst1.setInt(1, ejemplar.getInfectado());
            pst1.setInt(2, ejemplar.getN_encuentro());
            pst1.setInt(3, ejemplar.getN_pokedex());
            pst1.executeUpdate();
        } catch (SQLException throwables) {
            generateExceptionMessage(throwables);
        }
    }

    private void generateExceptionMessage(SQLException e) {
        logger.error("Message: {}", e.getMessage());
        logger.error("Code: {}", e.getErrorCode());
        logger.error("SQL State: {}", e.getSQLState());
    }

    private void generateExceptionMessage(Exception throwables) {
        logger.error("Message: {}", throwables.getMessage());
    }

}
